# This file creates docker containers of the GCC compiler
FROM ubuntu:20.04
MAINTAINER Matthias Moeller <mmoelle1@gmail.com>

# This file accepts the following build-time arguments:

# GCC_VERSION : Supported versions of the GCC compiler are 7, 8, 9, 10, 11, and 13
ARG GCC_VERSION=13

# Set the variable DEBIAN_FRONTEND to interactive. Don't use ENV since
# this sets the variable DEBIAN_FRONTEND also in the container.
ARG DEBIAN_FRONTEND=noninteractive

# Install prerequisite software
RUN apt-get update -q &&                         \
    apt-get install --no-install-recommends -yq  \
        alien                                    \
        apt-transport-https                      \
        beignet-opencl-icd                       \
        ca-certificates                          \
        clinfo                                   \
        cmake                                    \
        doxygen                                  \
        fftw-dev                                 \
        git                                      \
        graphviz                                 \
        libarrayfire-cpu-dev                     \
        libblas-dev                              \
        libboost-all-dev                         \
        liblapack-dev                            \
        mesa-common-dev                          \
        ninja-build                              \
        opencl-headers                           \
        subversion                               \
        unzip                                    \
        wget &&                                  \
    apt-get clean &&                             \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add ArrayFire to CMake search path
ENV ArrayFire_DIR /usr/lib/x86_64-linux-gnu/cmake/ArrayFire

# Add Boost library to CMake search path
ENV BOOST_LIBRARYDIR /usr/lib/x86_64-linux-gnu

# Initialize git command
RUN git config --global user.email "mmoelle1@gmail.com"
RUN git config --global user.name "Matthias Moeller"

# Install GCC version 7 (if required)
RUN if [ "$GCC_VERSION" = "7" ] ; then             \
    apt-get update -q &&                           \
    apt-get install --no-install-recommends -yq    \
        gcc-7                                      \
        g++-7 &&                                   \
    apt-get clean &&                               \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*; \
    fi

# Install GCC version 8 (if required)
RUN if [ "$GCC_VERSION" = "8" ] ; then             \
    apt-get update -q &&                           \
    apt-get install --no-install-recommends -yq    \
        gcc-8                                      \
        g++-8 &&                                   \
    apt-get clean &&                               \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*; \
    fi

# Install GCC version 9 (if required)
RUN if [ "$GCC_VERSION" = "9" ] ; then             \
    apt-get update -q &&                           \
    apt-get install --no-install-recommends -yq    \
        gcc-9                                      \
        g++-9 &&                                   \
    apt-get clean &&                               \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*; \
    fi

# Install GCC version 10 (if required)
RUN if [ "$GCC_VERSION" = "10" ] ; then            \
    apt-get update -q &&                           \
    apt-get install --no-install-recommends -yq    \
        gcc-10                                     \
        g++-10 &&                                  \
    apt-get clean &&                               \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*; \
    fi

# Install GCC version 11 (if required)
RUN if [ "$GCC_VERSION" = "11" ] ; then               \
    apt-get update -q &&                              \
    apt-get install --no-install-recommends -yq       \
        software-properties-common &&                 \
    add-apt-repository ppa:ubuntu-toolchain-r/test && \
    apt-get update -q &&                              \
    apt-get install --no-install-recommends -yq       \
        gcc-11                                        \
        g++-11 &&                                     \
    apt-get clean &&                                  \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*;    \
    fi

# Install GCC version 13 (if required)
RUN if [ "$GCC_VERSION" = "13" ] ; then               \
    apt-get update -q &&                              \
    apt-get install --no-install-recommends -yq       \
        software-properties-common &&                 \
    add-apt-repository ppa:ubuntu-toolchain-r/test && \
    apt-get update -q &&                              \
    apt-get install --no-install-recommends -yq       \
        gcc-13                                        \
        g++-13 &&                                     \
    apt-get clean &&                                  \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*;    \
    fi

# Manually set certificates of known SVN servers
RUN mkdir -p /root/.subversion/auth/svn.ssl.server/
COPY f1e6fb1c6ab2ebc37d6a8d735f7f2da0 /root/.subversion/auth/svn.ssl.server/

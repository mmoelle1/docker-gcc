# This file creates docker containers of the GCC compiler
FROM ubuntu:16.04
MAINTAINER Matthias Moeller <mmoelle1@gmail.com>

# This file accepts the following build-time arguments:

# GCC_VERSION : Supported versions of the GCC compiler are 4.7, 4.8, 4.9, 5
ARG GCC_VERSION=5

# Set the variable DEBIAN_FRONTEND to interactive. Don't use ENV since
# this sets the variable DEBIAN_FRONTEND also in the container.
ARG DEBIAN_FRONTEND=noninteractive

# Install prerequisite software
RUN apt-get update -q &&                         \
    apt-get install --no-install-recommends -yq  \
        alien                                    \
        apt-transport-https                      \
        ca-certificates                          \
        clinfo                                   \
        cmake                                    \
        doxygen                                  \
        fftw-dev                                 \
        git                                      \
        graphviz                                 \
        libarrayfire-cpu-dev                     \
        libblas-dev                              \
        libboost-all-dev                         \
        liblapack-dev                            \
        mesa-common-dev                          \
        ninja-build                              \
        ocl-icd-opencl-dev                       \
        opencl-headers                           \
        subversion                               \
        unzip                                    \
        wget &&                                  \
    apt-get clean &&                             \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add ArrayFire to CMake search path
ENV ArrayFire_DIR /usr/lib/x86_64-linux-gnu/cmake/ArrayFire

# Add Boost library to CMake search path
ENV BOOST_LIBRARYDIR /usr/lib/x86_64-linux-gnu

# Download and install the Intel OpenCL driver
RUN export OPENCL_DRIVER_URL="https://registrationcenter-download.intel.com/akdlm/irc_nas/vcp/15532/l_opencl_p_18.1.0.015.tgz" && \
    export TAR=$(basename ${OPENCL_DRIVER_URL}) &&                                                                               \
    export DIR=$(basename ${OPENCL_DRIVER_URL} .tgz) &&                                                                          \
    wget -q ${OPENCL_DRIVER_URL} &&                                                                                              \
    tar -xf ${TAR} &&                                                                                                            \
    sed -i 's/ACCEPT_EULA=decline/ACCEPT_EULA=accept/g' ${DIR}/silent.cfg &&                                                     \
    sh ${DIR}/install.sh --silent ${DIR}/silent.cfg --cli-mode &&                                                                \
    rm -rf ${TAR} ${DIR}
    
# Let the system know where to find the OpenCL library at runtime
ENV INTELOCLSDKROOT /opt/intel/opencl
ENV LIB ${INTELOCLSDKROOT}/lib64:$LIB
ENV LD_LIBRARY_PATH ${INTELOCLSDKROOT}/lib64:$LD_LIBRARY_PATH
ENV OpenCL_INCLUDE_DIR ${INTELOCLSDKROOT}/include
ENV OpenCL_LIBRARY ${INTELOCLSDKROOT}/lib64

# Initialize git command
RUN git config --global user.email "mmoelle1@gmail.com"
RUN git config --global user.name "Matthias Moeller"

# Install GCC version 4.7 (if required)
RUN if [ "$GCC_VERSION" = "4.7" ] ; then           \
    apt-get update -q &&                           \
    apt-get install --no-install-recommends -yq    \
        gcc-4.7                                    \
        g++-4.7 &&                                 \
    apt-get clean &&                               \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*; \
    fi

# Install GCC version 4.8 (if required)
RUN if [ "$GCC_VERSION" = "4.8" ] ; then           \
    apt-get update -q &&                           \
    apt-get install --no-install-recommends -yq    \
        gcc-4.8                                    \
        g++-4.8 &&                                 \
    apt-get clean &&                               \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*; \
    fi

# Install GCC version 4.9 (if required)
RUN if [ "$GCC_VERSION" = "4.9" ] ; then           \
    apt-get update -q &&                           \
    apt-get install --no-install-recommends -yq    \
        gcc-4.9                                    \
        g++-4.9 &&                                 \
    apt-get clean &&                               \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*; \
    fi

# Install GCC version 5 (if required)
RUN if [ "$GCC_VERSION" = "5" ] ; then             \
    apt-get update -q &&                           \
    apt-get install --no-install-recommends -yq    \
        gcc-5                                      \
        g++-5 &&                                   \
    apt-get clean &&                               \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*; \
    fi

# Manually set certificates of known SVN servers
RUN mkdir -p /root/.subversion/auth/svn.ssl.server/
COPY f1e6fb1c6ab2ebc37d6a8d735f7f2da0 /root/.subversion/auth/svn.ssl.server/
